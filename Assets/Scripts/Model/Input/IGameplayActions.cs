﻿namespace Model.Input {

    public interface IGameplayActions {

        void Roll(float dir);
        
        void Pitch(float dir);

        void Accelerate();

        void Decelerate();

        void Shoot();

    }

}