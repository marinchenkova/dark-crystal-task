﻿namespace Model.UI {

    public interface IEnemyIndicatorConfig {

        float ViewportOffset { get; }

    }

}