﻿using UnityEngine;

namespace Model.UI {

    public interface IEnemyIndicatorFactory {

        IEnemyIndicator Instantiate();

    }

}