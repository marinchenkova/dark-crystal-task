﻿using UnityEngine;

namespace Model.UI {

    public interface IEnemyIndicator {

        void SetTargetPosition(Vector3 position);
        
        void SetDistance(float distance);

        void SetIsOffScreen(bool isOffScreen);
        
        void Remove();

    }

}