﻿using Model.Base;
using Model.Motion;

namespace Model.Shooting {

    public class LinearMotion : IUpdatable {
        
        public float Speed { get; set; }

        private readonly IMovable _target;

        public LinearMotion(IMovable target) {
            _target = target;
        }

        public void Update() {
            _target.Move(Speed);
        }
        
    }

}