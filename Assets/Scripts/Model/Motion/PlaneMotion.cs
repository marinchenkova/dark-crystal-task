﻿using Model.Base;
using UnityEngine;

namespace Model.Motion {

    public class PlaneMotion : IPlaneMotion, IUpdatable {
        
        private readonly IPlaneMovable _target;
        private readonly IPlaneMotion _listener;
        private readonly IPlaneMotionConfig _config;

        private float _currentSpeed = 0f;
        private float _targetSpeed = 0f;
        private float _currentRoll = 0f;
        private float _currentPitch = 0f;
        
        public PlaneMotion(IPlaneMovable target, IPlaneMotion listener, IPlaneMotionConfig config) {
            _target = target;
            _listener = listener;
            _config = config;
            
            InitSpeed();
        }

        private void InitSpeed() {
            _currentSpeed = _config.Speed;
        }
        
        public void Accelerate() {
            _targetSpeed = _config.Speed * _config.AccelerateMultiplier;
            _listener.Accelerate();
        }

        public void Decelerate() {
            _targetSpeed = _config.Speed;
            _listener.Decelerate();
        }

        public void Roll(float dir) {
            _currentRoll = dir * _config.RollSpeed;
            _listener.Roll(dir);
        }

        public void Pitch(float dir) {
            _currentPitch = dir * _config.PitchSpeed;
            _listener.Pitch(dir);
        }

        public void Update() {
            _currentSpeed = Mathf.Lerp(_currentSpeed, _targetSpeed, Time.deltaTime * _config.AccelerationSpeed);
            
            _target.Move(_currentSpeed);
            _target.Roll(_currentRoll);
            _target.Pitch(_currentPitch);
        }

    }

}