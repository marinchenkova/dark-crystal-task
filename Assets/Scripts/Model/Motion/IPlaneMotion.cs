﻿namespace Model.Motion {

    public interface IPlaneMotion {

        void Accelerate();
        
        void Decelerate();

        void Roll(float dir);
        
        void Pitch(float dir);

    }

}