﻿namespace Model.Motion {

    public interface IPlaneMotionConfig {

        float Speed { get; }
        float AccelerateMultiplier { get; }
        float AccelerationSpeed { get; }
        float RollSpeed { get; }
        float PitchSpeed { get; }
        
    }

}