﻿namespace Model.Motion {

    public interface IPlaneMovable : IMovable {

        void Roll(float speed);
        
        void Pitch(float speed);

    }

}