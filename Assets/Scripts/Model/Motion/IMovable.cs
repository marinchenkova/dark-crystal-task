﻿namespace Model.Motion {

    public interface IMovable {

        void Move(float speed);

    }

}