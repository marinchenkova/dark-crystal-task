﻿namespace Model.Base {

    public interface IUpdatable {

        void Update();

    }

}