﻿using System;
using UnityEngine;

namespace Model.Base {

    public class Timer : IUpdatable {

        private Action _onFinish;
        private float _duration = 0f;
        private float _timer = 0f;
        private bool _isFinished = false;
        
        public void Setup(float duration, Action onFinish) {
            _onFinish = onFinish;
            _duration = duration;
            _timer = 0f;
            _isFinished = false;
        }

        public void Update() {
            if (_isFinished) return;

            _timer += Time.deltaTime;
            if (_timer >= _duration) {
                _isFinished = true;
                _onFinish.Invoke();
            }
        }
        
    }

}