﻿using UnityEngine;

namespace Model.Camera {

    public interface IThirdPersonCameraConfig {

        Vector3 MotionOffset { get; }
        float MotionSmooth { get; }
        
        float FovSmooth { get; }
        float InitialFov { get; }
        float AcceleratedFov { get; }

    }

}