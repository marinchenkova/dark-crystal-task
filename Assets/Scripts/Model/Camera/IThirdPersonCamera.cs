﻿using UnityEngine;

namespace Model.Camera {

    public interface IThirdPersonCamera {
        
        Vector3 PlayerPosition { get; }
        
        Quaternion PlayerRotation { get; }
        
        Vector3 CameraPosition { get; set; }
        
        Quaternion CameraRotation { get; set; }
        
        float Fov { get; set; }

    }

}