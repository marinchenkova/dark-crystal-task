﻿using Model.Base;
using UnityEngine;

namespace Model.Camera {

    public class ThirdPersonCameraFov : IUpdatable {
        
        private readonly IThirdPersonCamera _target;
        private readonly IThirdPersonCameraConfig _config;

        private float _targetFov = 0f;
        private float _currentFov = 0f;

        
        public ThirdPersonCameraFov(IThirdPersonCamera target, IThirdPersonCameraConfig config) {
            _target = target;
            _config = config;
            
            InitializeFov();
        }

        private void InitializeFov() {
            _targetFov = _config.InitialFov;
            _currentFov = _targetFov;
            _target.Fov = _targetFov;
        }
        
        public void Update() {
            var smooth = Time.deltaTime * _config.FovSmooth;
            _currentFov = Mathf.Lerp(_currentFov, _targetFov, smooth);
            _target.Fov = _currentFov;
        }

        public void OnAccelerate() {
            _targetFov = _config.AcceleratedFov;
        }

        public void OnDecelerate() {
            _targetFov = _config.InitialFov;
        }

    }

}