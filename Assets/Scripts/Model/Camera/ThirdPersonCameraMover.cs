﻿using Model.Base;
using UnityEngine;

namespace Model.Camera {

    public class ThirdPersonCameraMover : IUpdatable {
        
        private readonly IThirdPersonCamera _target;
        private readonly IThirdPersonCameraConfig _config;

        public ThirdPersonCameraMover(IThirdPersonCamera target, IThirdPersonCameraConfig config) {
            _target = target;
            _config = config;
        }

        public void InitializePosition() {
            var playerRotation = _target.PlayerRotation;
            _target.CameraPosition = _target.PlayerPosition + playerRotation * _config.MotionOffset;
            _target.CameraRotation = playerRotation;
        }
        
        public void Update() {
            Move();
            Rotate();
        }

        private void Rotate() {
            var cameraRotation = _target.CameraRotation;
            var smooth = Time.deltaTime * _config.MotionSmooth;
            var targetRotation = _target.PlayerRotation;
            var resultRotation = Quaternion.Lerp(cameraRotation, targetRotation, smooth);
            _target.CameraRotation = resultRotation;
        }

        private void Move() {
            var playerPosition = _target.PlayerPosition;
            var targetPosition = playerPosition + _target.PlayerRotation * _config.MotionOffset;
            _target.CameraPosition = targetPosition;
        }
        
    }

}