﻿using UnityEngine;

namespace Model.Shooting {

    public interface IShot {

        void Initialize(Vector3 position, Quaternion rotation, IShooterConfig config);

    }

}