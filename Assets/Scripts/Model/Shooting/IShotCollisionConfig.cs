﻿using UnityEngine;

namespace Model.Shooting {

    public interface IShotCollisionConfig {

        float MaxDistance { get; }
        
        LayerMask LayerMask { get; }

    }

}