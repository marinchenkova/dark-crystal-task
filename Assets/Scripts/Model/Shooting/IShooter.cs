﻿namespace Model.Shooting {

    public interface IShooter {

        void Shoot();

    }

}