﻿namespace Model.Shooting {

    public interface IShotReceiver {

        void OnShotReceived();

    }

}