﻿using Model.Base;
using UnityEngine;

namespace Model.Shooting {

    public class ShotCollisionDetector : IUpdatable {
        
        private readonly Transform _origin;
        private readonly IShotReceiver _listener;
        private readonly IShotCollisionConfig _config;

        private readonly RaycastHit[] _hits = new RaycastHit[1];

        public ShotCollisionDetector(
            Transform origin,
            IShotReceiver listener,
            IShotCollisionConfig config
        ) {
            _origin = origin;
            _listener = listener;
            _config = config;
        }
        
        public void Update() {
            var count = Physics.RaycastNonAlloc(
                _origin.position,
                _origin.up,
                _hits,
                _config.MaxDistance,
                _config.LayerMask,
                QueryTriggerInteraction.Ignore
            );
            if (count <= 0) return;
            
            var hit = _hits[0];
            
            var receiver = hit.transform.GetComponent<IShotReceiver>();
            if (receiver == null) return;
            
            receiver.OnShotReceived();
            _listener.OnShotReceived();
        }
        
    }

}