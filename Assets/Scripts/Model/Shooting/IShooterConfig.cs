﻿namespace Model.Shooting {

    public interface IShooterConfig {

        float ShotSpeed { get; }
        
        float ShotLifetime { get; }
        
    }

}