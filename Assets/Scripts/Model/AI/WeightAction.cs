﻿using System;

namespace Model.AI {

    public class WeightAction : IWeightAction {
        
        public IWeightActionConfig Config { get; }

        private readonly Action _action;

        public WeightAction(IWeightActionConfig config, Action action) {
            _action = action;
            Config = config;
        }

        public void Execute() {
            _action.Invoke();
        }

    }

}