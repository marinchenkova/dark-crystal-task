﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model.Base;
using Random = UnityEngine.Random;

namespace Model.AI {

    public class WeightActionExecutor {
        
        private readonly Timer _timer;
        private readonly List<IWeightAction> _actions;

        private float _sumWeight = 0f;
        
        private WeightActionExecutor(Timer timer, List<IWeightAction> actions) {
            _timer = timer;
            _actions = actions.OrderBy(action => action.Config.Weight).ToList();
            Init();
        }

        private void Init() {
            if (_actions.Count == 0) return;
            
            CalculateSumWeight();
            InitNext();
        }

        private void InitNext() {
            var action = PickNext();
            _timer.Setup(GetDuration(action), () => {
                action.Execute();
                InitNext();
            });
        }

        private void CalculateSumWeight() {
            foreach (var action in _actions) {
                _sumWeight += action.Config.Weight;
            }
        }

        private float GetDuration(IWeightAction action) {
            return Random.Range(action.Config.MinDuration, action.Config.MinDuration);
        }

        private IWeightAction PickNext() {
            var index = Random.value;
            var sum = 0f;
            
            foreach (var action in _actions) {
                var weight = action.Config.Weight;
                sum += weight;
                var indexSum = sum / _sumWeight;
                if (indexSum >= index) return action;
            }

            return _actions.Last();
        }

        public class Builder {

            private readonly List<IWeightAction> _actions = new List<IWeightAction>();
            
            public Builder Add(IWeightAction weightAction) {
                _actions.Add(weightAction);
                return this;
            }
            
            public Builder Add(IEnumerable<IWeightAction> actions) {
                _actions.AddRange(actions);
                return this;
            }
            
            public WeightActionExecutor WithTimer(Timer timer) {
                return new WeightActionExecutor(timer, _actions);
            }
            
        }
        
    }

}