﻿namespace Model.AI {

    public interface IWeightActionConfig {
        
        float Weight { get; }
        
        float MinDuration { get; }
        
        float MaxDuration { get; }

    }

}