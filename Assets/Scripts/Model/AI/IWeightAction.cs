﻿namespace Model.AI {

    public interface IWeightAction {

        IWeightActionConfig Config { get; }

        void Execute();

    }

}