﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model.Base;

namespace Controller.Base {

    public abstract class UpdatableController : IDisposable {

        private readonly List<IUpdatable> _updatables = new List<IUpdatable>();

        protected void SubscribeOnUpdate(IUpdatable updatable) {
            _updatables.Add(updatable);
        }
        
        protected void UnsubscribeOnUpdate(IUpdatable updatable) {
            _updatables.Remove(updatable);
        }
        
        public void OnUpdate() {
            foreach (var updatable in _updatables.ToList()) {
                updatable.Update();
            }
        }

        public void Dispose() {
            _updatables.Clear();
        }

    }

}