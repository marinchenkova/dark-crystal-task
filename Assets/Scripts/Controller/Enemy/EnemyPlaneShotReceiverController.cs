﻿using UnityEngine;
using View.Player;

namespace Controller.Player {

    public class EnemyPlaneShotReceiverController {
        private readonly GameObject _jet;
        private readonly GameObject _shotsPool;
        private readonly IPlaneEffectsView _effectsView;

        public EnemyPlaneShotReceiverController(GameObject jet, GameObject shotsPool, IPlaneEffectsView effectsView) {
            _jet = jet;
            _shotsPool = shotsPool;
            _effectsView = effectsView;
        }

        public void OnShotReceived() {
            _effectsView.OnShotReceived();
            Object.Destroy(_jet);
            Object.Destroy(_shotsPool);
        }
        
    }

}