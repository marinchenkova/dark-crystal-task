﻿using Controller.Base;
using Model.Base;
using Model.UI;
using UnityEngine;
using View;
using View.UI;

namespace Controller.Enemy {

    public class EnemyPlaneEffectsController : UpdatableController, IUpdatable {
        
        private readonly Transform _target;
        private readonly Transform _player;
        private readonly IEnemyIndicatorConfig _config;
        private readonly IPlaneAudioView _audioView;
        private readonly ParticleSystem _explosion;
        private readonly IEnemyIndicatorFactoryView _indicatorFactoryView;
        private readonly Camera _camera;
        private IEnemyIndicator _indicator;

        public EnemyPlaneEffectsController(
            Transform target,
            Transform player,
            IEnemyIndicatorConfig config,
            IPlaneAudioView audioView,
            ParticleSystem explosion,
            IEnemyIndicatorFactoryView indicatorFactoryView,
            Camera camera
        ) {
            _target = target;
            _player = player;
            _config = config;
            _audioView = audioView;
            _explosion = explosion;
            _indicatorFactoryView = indicatorFactoryView;
            _camera = camera;
            
        }

        public void InitIndicator() {
            _indicator = _indicatorFactoryView.Instantiate();
            SubscribeOnUpdate(this);
        }

        public void Update() {
            var targetPosition = _target.position;
            var distance = Vector3.Distance(_player.position, targetPosition);

            var viewportPoint = _camera.WorldToViewportPoint(targetPosition);
            var offset = _config.ViewportOffset;
            var isOffScreen = false;
            
            if (viewportPoint.z < 0f) {
                viewportPoint = Vector3.one - viewportPoint;
                viewportPoint.y = Mathf.Sign(viewportPoint.y - 0.5f) * 0.5f + 0.5f;
                isOffScreen = true;
            }
            if (viewportPoint.x < offset) {
                viewportPoint.x = offset;
                isOffScreen = true;
            }
            if (viewportPoint.x > 1f - offset) {
                viewportPoint.x = 1f - offset;
                isOffScreen = true;
            }
            if (viewportPoint.y < offset) {
                viewportPoint.y = offset;
                isOffScreen = true;
            }
            if (viewportPoint.y > 1f - offset) {
                viewportPoint.y = 1f - offset;
                isOffScreen = true;
            }

            var screenPoint = _camera.ViewportToScreenPoint(viewportPoint);

            _indicator.SetIsOffScreen(isOffScreen);
            _indicator.SetTargetPosition(screenPoint);
            _indicator.SetDistance(distance);
        }

        public void OnAccelerate() {
            _audioView.OnAccelerate();
        }
        
        public void OnDecelerate() {
            _audioView.OnDecelerate();
        }

        public void OnShoot() {
            _audioView.OnShoot();
        }

        public void OnShotReceived() {
            _indicator.Remove();
            UnsubscribeOnUpdate(this);
            _explosion.Play();
            _audioView.OnShotReceived();
        }
        
    }

}