﻿using Controller.Base;
using Model.AI;
using Model.Base;
using Model.Motion;
using View.Motion;
using View.Player;
using View.Shooting;

namespace Controller.Enemy {

    public class EnemyPlaneController : UpdatableController {

        private readonly IPlaneMotion _planeMotion;
        private readonly IShooterView _shooterView;
        private readonly IWeightActionConfig _actionConfig;

        public EnemyPlaneController(
            IPlaneMovableView view,
            IPlaneEffectsView effectsView,
            IShooterView shooterView,
            IPlaneMotionConfig motionConfig,
            IWeightActionConfig actionConfig
        ) {
            var planeMotion = new PlaneMotion(view, effectsView, motionConfig);
            SubscribeOnUpdate(planeMotion);
            
            _planeMotion = planeMotion;
            _shooterView = shooterView;
            _actionConfig = actionConfig;

            InitAi();
        }

        public void InitSpeed() {
            _planeMotion.Decelerate();
        }

        private void InitAi() {
            var timer = new Timer();
            new WeightActionExecutor.Builder()
                .Add(new WeightAction(_actionConfig, _planeMotion.Accelerate))
                .Add(new WeightAction(_actionConfig, _planeMotion.Decelerate))
                .Add(new WeightAction(_actionConfig, () => _planeMotion.Pitch(0f)))
                .Add(new WeightAction(_actionConfig, () => _planeMotion.Pitch(1f)))
                .Add(new WeightAction(_actionConfig, () => _planeMotion.Pitch(-1f)))
                .Add(new WeightAction(_actionConfig, () => _planeMotion.Roll(0f)))
                .Add(new WeightAction(_actionConfig, () => _planeMotion.Roll(1f)))
                .Add(new WeightAction(_actionConfig, () => _planeMotion.Roll(-1f)))
                .Add(new WeightAction(_actionConfig, _shooterView.Shoot))
                .WithTimer(timer);
            
            SubscribeOnUpdate(timer);
        }

    }

}