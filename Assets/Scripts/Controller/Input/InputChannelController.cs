﻿using System;
using System.Collections.Generic;
using Model.Base;
using Model.Input;
using UnityEngine;
using UnityEngine.InputSystem;
using View.Input;

namespace Controller.Input {

    public class InputChannelController : IPublisher<IGameplayActions>, InputActions.IGameplayActions, IDisposable {

        private readonly HashSet<IGameplayActions> _listeners = new HashSet<IGameplayActions>();

        public void Subscribe(IGameplayActions listener) {
            _listeners.Add(listener);
        }

        public void Unsubscribe(IGameplayActions listener) {
            _listeners.Remove(listener);
        }

        public void OnPitch(InputAction.CallbackContext context) {
            var dir = context.ReadValue<float>();
            foreach (var listener in _listeners) {
                listener.Pitch(dir);
            }
        }

        public void OnRoll(InputAction.CallbackContext context) {
            var dir = context.ReadValue<float>();
            foreach (var listener in _listeners) {
                listener.Roll(dir);
            }
        }

        public void OnAccelerate(InputAction.CallbackContext context) {
            if (context.started) {
                foreach (var listener in _listeners) {
                    listener.Accelerate();
                }   
                return;
            }

            if (context.canceled) {
                foreach (var listener in _listeners) {
                    listener.Decelerate();
                }
            }
        }

        public void OnShoot(InputAction.CallbackContext context) {
            if (context.started) {
                foreach (var listener in _listeners) {
                    listener.Shoot();
                }
            }
        }

        public void Dispose() {
            _listeners.Clear();
        }
        
    }

}