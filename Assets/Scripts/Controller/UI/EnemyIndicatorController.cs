﻿using UnityEngine;
using UnityEngine.UI;

namespace Controller.UI {

    public class EnemyIndicatorController {
        
        private readonly Canvas _parent;
        private readonly Image _pointer;
        private readonly Image _arrow;
        private readonly Text _distanceText;
        private readonly RectTransform _target;

        public EnemyIndicatorController(Image pointer, Image arrow, Text distanceText, RectTransform target) {
            _pointer = pointer;
            _arrow = arrow;
            _distanceText = distanceText;
            _target = target;
        }

        public void SetTargetPosition(Vector3 position) {
            _target.position = position;
        }

        public void SetDistance(float distance) {
            _distanceText.text = distance.ToString("F1");
        }

        public void SetIsOffScreen(bool isOffScreen) {
            _distanceText.enabled = !isOffScreen;
            _pointer.enabled = !isOffScreen;
            _arrow.enabled = isOffScreen;
        }
        
    }

}