﻿using Model.UI;
using UnityEngine;
using View.UI;

namespace Controller.UI {

    public class EnemyIndicatorFactoryController {
        
        private readonly RectTransform _parent;
        private readonly GameObject _indicator;

        public EnemyIndicatorFactoryController(RectTransform parent, GameObject indicator) {
            _parent = parent;
            _indicator = indicator;
        }

        public IEnemyIndicator Instantiate() {
            var newIndicator = Object.Instantiate(_indicator, _parent);
            return newIndicator.GetComponent<IEnemyIndicatorView>();
        }
        
    }

}