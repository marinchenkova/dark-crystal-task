﻿using View;
using View.Camera;

namespace Controller.Player {

    public class PlayerEffectsController {
        
        private readonly IPlaneAudioView _audioView;
        private readonly IThirdPersonCameraView _cameraView;

        public PlayerEffectsController(
            IPlaneAudioView audioView,
            IThirdPersonCameraView cameraView
        ) {
            _audioView = audioView;
            _cameraView = cameraView;
        }

        public void OnAccelerate() {
            _cameraView.OnAccelerate();
            _audioView.OnAccelerate();
        }
        
        public void OnDecelerate() {
            _cameraView.OnDecelerate();
            _audioView.OnDecelerate();
        }

        public void OnShoot() {
            _audioView.OnShoot();
        }

        public void OnShotReceived() {
            _audioView.OnShotReceived();
        }

    }

}