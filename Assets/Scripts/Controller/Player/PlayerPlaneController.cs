﻿using Controller.Base;
using Model.Input;
using Model.Motion;
using UnityEngine;
using View;
using View.Motion;
using View.Player;
using View.Shooting;

namespace Controller.Player {

    public class PlayerPlaneController : UpdatableController, IGameplayActions {
        
        private readonly IShooterView _shooterView;
        private readonly IPlaneMotion _planeMotion;
        private readonly Transform _motionTarget;
        
        public PlayerPlaneController(
            IPlaneMovableView view,
            IPlaneEffectsView effectsView,
            IShooterView shooterView,
            IPlaneMotionConfig motionConfig
        ) {
            _shooterView = shooterView;
            var planeMotion = new PlaneMotion(view, effectsView, motionConfig);
            _planeMotion = planeMotion;
            SubscribeOnUpdate(planeMotion);
        }

        public void InitSpeed() {
            _planeMotion.Decelerate();
        }
        
        public void Roll(float dir) {
            _planeMotion.Roll(dir);
        }

        public void Pitch(float dir) {
            _planeMotion.Pitch(dir);
        }

        public void Accelerate() {
            _planeMotion.Accelerate();
        }

        public void Decelerate() {
            _planeMotion.Decelerate();
        }

        public void Shoot() {
            _shooterView.Shoot();
        }
        
    }

}