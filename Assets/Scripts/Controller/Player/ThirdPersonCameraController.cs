﻿using Controller.Base;
using Model.Camera;
using UnityEngine;
using View.Camera;

namespace Controller.Player {

    public class ThirdPersonCameraController : UpdatableController {
        
        private readonly ThirdPersonCameraMover _mover;
        private readonly ThirdPersonCameraFov _fov;

        public ThirdPersonCameraController(
            IThirdPersonCameraView view,
            IThirdPersonCameraConfig config
        ) {
            _mover = new ThirdPersonCameraMover(view, config);
            _fov = new ThirdPersonCameraFov(view, config);
            Cursor.lockState = CursorLockMode.Locked;
            SubscribeOnUpdate(_mover);
            SubscribeOnUpdate(_fov);
        }

        public void InitializePosition() {
            _mover.InitializePosition();
        }

        public void OnAccelerate() {
            _fov.OnAccelerate();
        }
        
        public void OnDecelerate() {
            _fov.OnDecelerate();
        }
        
    }

}