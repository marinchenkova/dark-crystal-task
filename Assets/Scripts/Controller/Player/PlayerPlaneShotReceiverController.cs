﻿using View.Player;

namespace Controller.Player {

    public class PlayerPlaneShotReceiverController {
        
        private readonly IPlaneEffectsView _effectsView;

        public PlayerPlaneShotReceiverController(IPlaneEffectsView effectsView) {
            _effectsView = effectsView;
        }

        public void OnShotReceived() {
            _effectsView.OnShotReceived();
        }
        
    }

}