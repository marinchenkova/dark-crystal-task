﻿using System.Collections.Generic;
using UnityEngine;
using View.Pool;

namespace Controller.Pool {

    public class ObjectPoolController {
        
        private readonly PoolElement _sample;
        private readonly Transform _parent;

        private readonly HashSet<PoolElement> _pool = new HashSet<PoolElement>();

        public ObjectPoolController(PoolElement sample, Transform parent) {
            _sample = sample;
            _parent = parent;
        }

        public void InitPool(int amount) {
            for (var i = 0; i < amount; i++) {
                _pool.Add(Create());
            }
        }

        public PoolElement GetNext() {
            foreach (var next in _pool) {
                if (!next.IsActive) {
                    next.IsActive = true;
                    return next;
                }
            }

            var another = Create();
            another.IsActive = true;
            _pool.Add(another);
            
            return another;
        }

        private PoolElement Create() {
            var temp = Object.Instantiate(_sample, _parent, true);
            temp.IsActive = false;
            return temp;
        }

    }

}