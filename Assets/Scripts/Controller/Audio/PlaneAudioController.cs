﻿using UnityEngine;
using View.Effects;

namespace Controller.Audio {

    public class PlaneAudioController {
        
        private readonly AudioSource _motionSource;
        private readonly AudioSource _fxSource;
        private readonly AudioHighPassFilter _filter;
        private readonly IPlaneSoundsConfig _config;

        public PlaneAudioController(
            AudioSource motionSource,
            AudioSource fxSource,
            AudioHighPassFilter filter,
            IPlaneSoundsConfig config
        ) {
            _motionSource = motionSource;
            _fxSource = fxSource;
            _filter = filter;
            _config = config;
        }

        public void Initialize() {
            _motionSource.clip = _config.Motion.Sample;
            _motionSource.volume = _config.Motion.Volume;
            _motionSource.loop = true;
            _motionSource.Play();
        }
        
        public void OnAccelerate() {
            _filter.cutoffFrequency = _config.AccelerateHighPassFreq;
        }

        public void OnDecelerate() {
            _filter.cutoffFrequency = _config.DecelerateHighPassFreq;
        }

        public void OnShoot() {
            _fxSource.PlayOneShot(_config.Shoot.Sample, _config.Shoot.Volume);
        }

        public void OnShotReceived() {
            _fxSource.PlayOneShot(_config.Explode.Sample, _config.Explode.Volume);
        }

        public void Stop() {
            _fxSource.Stop();
            _motionSource.Stop();
        }
        
    }

}