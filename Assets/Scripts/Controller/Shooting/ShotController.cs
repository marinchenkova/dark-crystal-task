﻿using Controller.Base;
using Model.Base;
using Model.Shooting;
using UnityEngine;
using View.Pool;
using View.Shooting;

namespace Controller.Shooting {

    public class ShotController : UpdatableController {

        private readonly IShotEffectsView _effectsView;
        private readonly Transform _target;
        private readonly PoolElement _poolElement;
        private readonly Timer _timer;
        private readonly LinearMotion _motion;
        private readonly ShotCollisionDetector _detector;

        public ShotController(
            IShotView view, 
            IShotEffectsView effectsView, 
            Transform target, 
            PoolElement poolElement,
            IShotCollisionConfig config
        ) {
            _effectsView = effectsView;
            _target = target;
            _poolElement = poolElement;

            _timer = new Timer();
            _motion = new LinearMotion(view);
            _detector = new ShotCollisionDetector(target, view, config);
        }

        public void Initialize(Vector3 position, Quaternion rotation, IShooterConfig config) {
            _target.SetPositionAndRotation(position, rotation);
            _motion.Speed = config.ShotSpeed;
            _timer.Setup(config.ShotLifetime, Crash);
            
            SubscribeOnUpdate(_timer);
            SubscribeOnUpdate(_motion);
            SubscribeOnUpdate(_detector);
            
            _effectsView.OnInit();
        }

        public void Crash() {
            _effectsView.OnShotReceived();
            _poolElement.IsActive = false;
            Dispose();
        }
        
    }

}