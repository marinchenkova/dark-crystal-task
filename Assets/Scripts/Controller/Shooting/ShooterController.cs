﻿using Model.Shooting;
using UnityEngine;
using View.Player;
using View.Pool;

namespace Controller.Shooting {

    public class ShooterController {
        
        private readonly ObjectPool _shotsPool;
        private readonly IPlaneEffectsView _effectsView;
        private readonly Transform _startPoint1;
        private readonly Transform _startPoint2;
        private readonly IShooterConfig _config;

        private bool _shootOnPoint1 = true;
        
        public ShooterController(
            ObjectPool shotsPool,
            IPlaneEffectsView effectsView,
            Transform startPoint1,
            Transform startPoint2,
            IShooterConfig config
        ) {
            _shotsPool = shotsPool;
            _effectsView = effectsView;
            _startPoint1 = startPoint1;
            _startPoint2 = startPoint2;
            _config = config;
        }
        
        public void Shoot() {
            var shot = _shotsPool.GetNextPoolElement().GetComponent<IShot>();
            
            if (_shootOnPoint1) {
                shot.Initialize(_startPoint1.position, _startPoint1.rotation, _config);    
            }
            else {
                shot.Initialize(_startPoint2.position, _startPoint2.rotation, _config);    
            }
            _effectsView.Shoot();
            _shootOnPoint1 = !_shootOnPoint1;
        }

    }

}