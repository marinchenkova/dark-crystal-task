﻿using Model.Motion;
using UnityEngine;

namespace View.Motion {

    [CreateAssetMenu(fileName = "PlaneMotionConfig", menuName = "App/PlaneMotionConfig")]
    public class PlaneMotionConfig : ScriptableObject, IPlaneMotionConfig {

        public float Speed => _speed;
        public float AccelerateMultiplier => _accelerateMultiplier;
        public float AccelerationSpeed => _accelerationSpeed;
        public float RollSpeed => _rollSpeed;
        public float PitchSpeed => _pitchSpeed;
        
        [SerializeField]
        private float _speed = 0f;
        
        [SerializeField]
        private float _accelerateMultiplier = 0f;

        [SerializeField]
        private float _accelerationSpeed = 0f;
        
        [SerializeField]
        private float _rollSpeed = 0f;
        
        [SerializeField]
        private float _pitchSpeed = 0f;

    }
    
}