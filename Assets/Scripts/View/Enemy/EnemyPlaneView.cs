﻿using System;
using Controller.Enemy;
using UnityEngine;
using View.AI;
using View.Motion;
using View.Shooting;

namespace View.Enemy {

    public class EnemyPlaneView : MonoBehaviour, IPlaneMovableView {

        [SerializeField]
        private Transform _motionTarget;

        [SerializeField]
        private PlaneEffectsView _effectsView;
        
        [SerializeField]
        private ShooterView _shooterView;
        
        [SerializeField]
        private PlaneMotionConfig _motionConfig;

        [SerializeField]
        private WeightActionConfig _actionConfig;

        private EnemyPlaneController _controller;

        private void Awake() {
            _controller = new EnemyPlaneController(this, _effectsView, _shooterView, _motionConfig, _actionConfig);
        }

        private void Start() {
            _controller.InitSpeed();
        }

        private void OnDestroy() {
            _controller.Dispose();
        }

        private void Update() {
            _controller.OnUpdate();
        }
        
        public void Move(float speed) {
            var resultSpeed = speed * Time.deltaTime;
            _motionTarget.Translate(Vector3.forward * resultSpeed, Space.Self);
        }

        public void Roll(float speed) {
            var resultSpeed = speed * Time.deltaTime;
            _motionTarget.Rotate(0f, 0f, resultSpeed, Space.Self);
        }

        public void Pitch(float speed) {
            var resultSpeed = speed * Time.deltaTime;
            _motionTarget.Rotate(resultSpeed, 0f, 0f, Space.Self);
        }

    }

}