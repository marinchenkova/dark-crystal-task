﻿using System;
using Controller.Enemy;
using UnityEngine;
using View.Shooting;
using View.UI;

namespace View.Enemy {

    public class EnemyPlaneEffectsView : PlaneEffectsView {

        [SerializeField]
        private PlaneAudioView _planeAudioView;

        [SerializeField]
        private ParticleSystem _explosion;

        [SerializeField]
        private Transform _target;
        
        [SerializeField]
        private Transform _player;

        [SerializeField]
        private EnemyIndicatorConfig _indicatorConfig;

        [SerializeField] 
        private UnityEngine.Camera _camera;
        
        [SerializeField] 
        private EnemyIndicatorFactoryView _indicatorFactoryView;
        
        private EnemyPlaneEffectsController _controller;

        private void Awake() {
            _controller = new EnemyPlaneEffectsController(
                _target,
                _player,
                _indicatorConfig,
                _planeAudioView,
                _explosion,
                _indicatorFactoryView,
                _camera
            );
        }

        private void Start() {
            _controller.InitIndicator();
        }

        private void Update() {
            _controller.OnUpdate();
        }

        private void OnDestroy() {
            _controller.Dispose();
        }

        public override void Accelerate() {
            _controller.OnAccelerate();
        }

        public override void Decelerate() {
            _controller.OnDecelerate();
        }

        public override void Shoot() {
            _controller.OnShoot();
        }

        public override void OnShotReceived() {
            _controller.OnShotReceived();
        }

        public override void Roll(float dir) { }

        public override void Pitch(float dir) { }
        
    }

}