﻿using Controller.Player;
using UnityEngine;
using View.Shooting;

namespace View.Enemy {

    public class EnemyPlaneShotReceiverView : MonoBehaviour, IShotReceiverView {

        [SerializeField]
        private GameObject _jet;
        
        [SerializeField]
        private GameObject _shotsPool;

        [SerializeField]
        private PlaneEffectsView _effectsView;
        
        private EnemyPlaneShotReceiverController _controller;
        
        private void Awake() {
            _controller = new EnemyPlaneShotReceiverController(_jet, _shotsPool, _effectsView);
        }

        public void OnShotReceived() {
            _controller.OnShotReceived();
        }
        
    }

}