﻿using Model.Shooting;

namespace View.Shooting {

    public interface IShotEffectsView : IShotReceiver {
        
        void OnInit();
        
    }

}