﻿using Model.Motion;
using Model.Shooting;

namespace View.Shooting {

    public interface IShotView : IShot, IMovable, IShotReceiver {

    }

}