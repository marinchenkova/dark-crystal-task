﻿using Model.Shooting;
using UnityEngine;

namespace View.Shooting {

    [CreateAssetMenu(fileName = "ShooterConfig", menuName = "App/ShooterConfig")]
    public class ShooterConfig : ScriptableObject, IShooterConfig {

        public float ShotSpeed => _shotSpeed;
        public float ShotLifetime => _shotLifetime;

        [SerializeField]
        private float _shotSpeed;

        [SerializeField]
        private float _shotLifetime;

    }

}