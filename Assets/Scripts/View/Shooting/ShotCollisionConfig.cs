﻿using Model.Shooting;
using UnityEngine;

namespace View.Shooting {

    [CreateAssetMenu(fileName = "ShotCollisionConfig", menuName = "App/ShotCollisionConfig")]
    public class ShotCollisionConfig : ScriptableObject, IShotCollisionConfig {

        public float MaxDistance => _maxDistance;
        public LayerMask LayerMask => _layerMask;

        [SerializeField]
        private float _maxDistance;

        [SerializeField]
        private LayerMask _layerMask;
        
    }

}