﻿using Controller.Shooting;
using UnityEngine;
using View.Pool;

namespace View.Shooting {

    public class ShooterView : MonoBehaviour, IShooterView {

        [SerializeField]
        private ObjectPool _shotsPool;
        
        [SerializeField] 
        private ShooterConfig _config;

        [SerializeField]
        private Transform _startPoint1;
        
        [SerializeField]
        private Transform _startPoint2;

        [SerializeField]
        private PlaneEffectsView _effectsView;
        
        private ShooterController _controller;

        private void Awake() {
            _controller = new ShooterController(_shotsPool, _effectsView, _startPoint1, _startPoint2, _config);
        }

        public void Shoot() {
            _controller.Shoot();
        }
        
    }

}