﻿using Controller.Shooting;
using Model.Shooting;
using UnityEngine;
using View.Pool;

namespace View.Shooting {

    public class ShotView : MonoBehaviour, IShotView {
        
        [SerializeField]
        private PoolElement _poolElement;

        [SerializeField]
        private Transform _motionTarget;

        [SerializeField]
        private ShotEffectsView _effectsView;

        [SerializeField]
        private ShotCollisionConfig _config;
        
        private ShotController _controller;
        
        private void Awake() {
            _controller = new ShotController(this, _effectsView, _motionTarget, _poolElement, _config);
        }

        private void Update() {
            _controller.OnUpdate();
        }

        private void OnDestroy() {
            _controller.Dispose();
        }

        public void Initialize(Vector3 position, Quaternion rotation, IShooterConfig config) {
            _controller.Initialize(position, rotation, config);
        }

        public void OnShotReceived() {
            _controller.Crash();
        }

        public void Move(float speed) {
            var resultSpeed = speed * Time.deltaTime;
            _motionTarget.Translate(Vector3.up * resultSpeed, Space.Self);
        }
        
    }

}