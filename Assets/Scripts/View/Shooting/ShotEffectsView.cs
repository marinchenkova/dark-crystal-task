﻿using Controller.Shooting;
using UnityEngine;

namespace View.Shooting {

    public class ShotEffectsView : MonoBehaviour, IShotEffectsView {

        private ShotEffectsController _controller;
        
        private void Awake() {
            _controller = new ShotEffectsController();
        }

        public void OnInit() {
            
        }

        public void OnShotReceived() {
            
        }

    }

}