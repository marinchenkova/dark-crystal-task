﻿using Model.AI;
using UnityEngine;

namespace View.AI {

    [CreateAssetMenu(fileName = "WeightActionConfig", menuName = "App/WeightActionConfig")]
    public class WeightActionConfig : ScriptableObject, IWeightActionConfig {

        public float Weight => _weight;
        public float MinDuration => _minDuration;
        public float MaxDuration => _maxDuration;

        [SerializeField]
        private float _weight;
        
        [SerializeField]
        private float _minDuration;

        [SerializeField]
        private float _maxDuration;

    }

}