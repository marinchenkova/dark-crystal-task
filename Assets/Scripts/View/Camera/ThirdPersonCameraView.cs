﻿using System;
using Controller.Player;
using UnityEngine;
using View.Input;

namespace View.Camera {

    public class ThirdPersonCameraView : MonoBehaviour, IThirdPersonCameraView {

        [SerializeField]
        private Transform _player;

        [SerializeField]
        private UnityEngine.Camera _camera;

        [SerializeField]
        private ThirdPersonCameraConfig _config;

        public Vector3 PlayerPosition => _player.position;
        public Quaternion PlayerRotation => _player.rotation;

        public Vector3 CameraPosition {
            get => _cameraTransform.position;
            set => _cameraTransform.position = value;
        }

        public Quaternion CameraRotation {
            get => _cameraTransform.rotation;
            set => _cameraTransform.rotation = value;
        }

        public float Fov { 
            get => _camera.fieldOfView;
            set => _camera.fieldOfView = value;
        }

        private ThirdPersonCameraController _controller;
        private Transform _cameraTransform;
        
        private void Awake() {
            _cameraTransform = _camera.transform;
            _controller = new ThirdPersonCameraController(this, _config);
        }

        private void Start() {
            _controller.InitializePosition();
        }

        private void OnDestroy() {
            _controller.Dispose();
        }

        private void LateUpdate() {
            _controller.OnUpdate();
        }

        public void OnAccelerate() {
            _controller.OnAccelerate();
        }

        public void OnDecelerate() {
            _controller.OnDecelerate();
        }

        public void OnShotReceived() {
            
        }
        
    }

}