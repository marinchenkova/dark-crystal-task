﻿using Model.Camera;

namespace View.Camera {

    public interface IThirdPersonCameraView : IThirdPersonCamera {

        void OnAccelerate();

        void OnDecelerate();

    }

}