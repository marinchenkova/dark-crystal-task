﻿using Model.Camera;
using UnityEngine;

namespace View.Camera {

    [CreateAssetMenu(fileName = "ThirdPersonCameraConfig", menuName = "App/ThirdPersonCameraConfig")]
    public class ThirdPersonCameraConfig : ScriptableObject, IThirdPersonCameraConfig {

        public Vector3 MotionOffset => _offset;
        public float MotionSmooth => _smooth;
        public float FovSmooth => _fovSmooth;
        public float InitialFov => _initialFov;
        public float AcceleratedFov => _acceleratedFov;

        [SerializeField]
        private Vector3 _offset;

        [SerializeField]
        private float _smooth;
        
        [SerializeField]
        private float _fovSmooth;
        
        [SerializeField]
        private float _initialFov;

        [SerializeField]
        private float _acceleratedFov;

    }

}