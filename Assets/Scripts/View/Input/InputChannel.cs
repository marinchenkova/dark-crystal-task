﻿using System;
using Controller.Input;
using Model.Base;
using Model.Input;
using UnityEngine;

namespace View.Input {
    
    public class InputChannel : MonoBehaviour {
    
        public IPublisher<IGameplayActions> GameplayActions => _controller;
        
        private readonly InputChannelController _controller = new InputChannelController();
        private InputActions _inputActions;

        private void Awake() {
            _inputActions = new InputActions();
            _inputActions.Gameplay.SetCallbacks(_controller);
        }

        private void OnEnable() {
            _inputActions.Enable();
        }

        private void OnDisable() {
            _inputActions.Disable();
        }

        private void OnDestroy() {
            _inputActions.Dispose();
            _controller.Dispose();
        }
        
    }
    
}