﻿using Controller.Player;
using UnityEngine;
using View.Camera;
using View.Player;

namespace View.Motion {

    public class PlayerPlaneEffectsView : PlaneEffectsView {

        [SerializeField]
        private ThirdPersonCameraView _thirdPersonCameraView;

        [SerializeField]
        private PlaneAudioView _planeAudioView;

        private PlayerEffectsController _controller;

        private void Awake() {
            _controller = new PlayerEffectsController(_planeAudioView, _thirdPersonCameraView);
        }

        public override void Accelerate() {
            _controller.OnAccelerate();
        }

        public override void Decelerate() {
            _controller.OnDecelerate();
        }

        public override void Shoot() {
            _controller.OnShoot();
        }

        public override void OnShotReceived() {
            _controller.OnShotReceived();
        }

        public override void Roll(float dir) { }

        public override void Pitch(float dir) { }
        
    }

}