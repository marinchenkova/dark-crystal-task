﻿using System;
using Controller.Player;
using UnityEngine;
using View.Shooting;

namespace View.Player {

    public class PlayerPlaneShotReceiverView : MonoBehaviour, IShotReceiverView {

        [SerializeField]
        private PlaneEffectsView _effectsView;

        private PlayerPlaneShotReceiverController _controller;

        private void Awake() {
            _controller = new PlayerPlaneShotReceiverController(_effectsView);
        }

        public void OnShotReceived() {
            _controller.OnShotReceived();
        }

    }

}