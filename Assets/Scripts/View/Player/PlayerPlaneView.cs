﻿using Controller.Player;
using UnityEngine;
using View.Input;
using View.Motion;
using View.Shooting;

namespace View {

    public class PlayerPlaneView : MonoBehaviour, IPlaneMovableView {

        [SerializeField]
        private Transform _motionTarget;

        [SerializeField]
        private PlaneEffectsView _effectsView;
        
        [SerializeField]
        private ShooterView _shooterView;
        
        [SerializeField]
        private PlaneMotionConfig _motionConfig;

        [SerializeField]
        private InputChannel _inputChannel;
        
        private PlayerPlaneController _controller;
        
        private void Awake() {
            _controller = new PlayerPlaneController(this, _effectsView, _shooterView, _motionConfig);
        }

        private void OnEnable() {
            _inputChannel.GameplayActions.Subscribe(_controller);
        }

        private void Start() {
            _controller.InitSpeed();
        }
        
        private void OnDisable() {
            _inputChannel.GameplayActions.Unsubscribe(_controller);
        }

        private void OnDestroy() {
            _controller.Dispose();
        }

        private void Update() {
            _controller.OnUpdate();
        }

        public void Move(float speed) {
            var resultSpeed = speed * Time.deltaTime;
            _motionTarget.Translate(Vector3.forward * resultSpeed, Space.Self);
        }

        public void Roll(float speed) {
            var resultSpeed = speed * Time.deltaTime;
            _motionTarget.Rotate(0f, 0f, resultSpeed, Space.Self);
        }

        public void Pitch(float speed) {
            var resultSpeed = speed * Time.deltaTime;
            _motionTarget.Rotate(resultSpeed, 0f, 0f, Space.Self);
        }
        
    }

}