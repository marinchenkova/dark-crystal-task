﻿using Controller.Pool;
using UnityEngine;

namespace View.Pool {

    public class ObjectPool : MonoBehaviour {

        [SerializeField]
        private PoolElement _sample;

        [SerializeField]
        private int _initialAmount;
        
        private ObjectPoolController _controller;

        private void Awake() {
            _controller = new ObjectPoolController(_sample, transform);
        }

        private void Start() {
            _controller.InitPool(_initialAmount);
        }

        public PoolElement GetNextPoolElement() {
            return _controller.GetNext();
        }
        
    }

}