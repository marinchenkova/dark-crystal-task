﻿using UnityEngine;

namespace View.Pool {

    public class PoolElement : MonoBehaviour {
        
        public bool IsActive {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }
        
    }

}