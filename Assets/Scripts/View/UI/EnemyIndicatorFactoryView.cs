﻿using Controller.UI;
using Model.UI;
using UnityEngine;

namespace View.UI {

    public class EnemyIndicatorFactoryView : MonoBehaviour, IEnemyIndicatorFactoryView {

        [SerializeField]
        private GameObject _indicator;

        [SerializeField]
        private RectTransform _parent;

        private EnemyIndicatorFactoryController _controller;

        private void Awake() {
            _controller = new EnemyIndicatorFactoryController(_parent, _indicator);
        }

        public IEnemyIndicator Instantiate() {
            return _controller.Instantiate();
        }
        
    }

}