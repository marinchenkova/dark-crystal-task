﻿using Model.UI;
using UnityEngine;

namespace View.Shooting {

    [CreateAssetMenu(fileName = "EnemyIndicatorConfig", menuName = "App/EnemyIndicatorConfig")]
    public class EnemyIndicatorConfig : ScriptableObject, IEnemyIndicatorConfig {

        public float ViewportOffset => _viewportOffset;

        [SerializeField]
        private float _viewportOffset;

    }

}