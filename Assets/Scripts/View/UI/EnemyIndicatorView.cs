﻿using System;
using Controller.UI;
using UnityEngine;
using UnityEngine.UI;

namespace View.UI {

    public class EnemyIndicatorView : MonoBehaviour, IEnemyIndicatorView {

        [SerializeField]
        private Text _distanceText;

        [SerializeField]
        private RectTransform _target;

        [SerializeField]
        private Image _pointer;

        [SerializeField]
        private Image _arrow;
        
        private EnemyIndicatorController _controller;
        
        private void Awake() {
            _controller = new EnemyIndicatorController(_pointer, _arrow, _distanceText, _target);
        }

        public void SetTargetPosition(Vector3 position) {
            _controller.SetTargetPosition(position);
        }

        public void SetDistance(float distance) {
            _controller.SetDistance(distance);
        }

        public void SetIsOffScreen(bool isOffScreen) {
            _controller.SetIsOffScreen(isOffScreen);
        }

        public void Remove() {
            Destroy(gameObject);
        }
        
    }

}