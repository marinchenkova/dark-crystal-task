﻿using UnityEngine;
using View.Player;

namespace View {

    public abstract class PlaneEffectsView : MonoBehaviour, IPlaneEffectsView {
        
        public abstract void Shoot();
        
        public abstract void OnShotReceived();
        
        public abstract void Accelerate();
        
        public abstract void Decelerate();
        
        public abstract void Roll(float dir);
        
        public abstract void Pitch(float dir);
        
    }

}