﻿﻿namespace View {

    public interface IPlaneAudioView {

        void OnAccelerate();

        void OnDecelerate();

        void OnShoot();

        void OnShotReceived();

    }

}