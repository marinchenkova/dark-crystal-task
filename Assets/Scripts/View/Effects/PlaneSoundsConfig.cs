﻿using UnityEngine;

namespace View.Effects {

    [CreateAssetMenu(fileName = "PlaneSoundsConfig", menuName = "App/PlaneSoundsConfig")]
    public class PlaneSoundsConfig : ScriptableObject, IPlaneSoundsConfig {

        public float AccelerateHighPassFreq => _accelerateHighPassFreq;
        public float DecelerateHighPassFreq => _decelerateHighPassFreq;
        
        public VolumedAudioClip Motion => _motion;
        public VolumedAudioClip Shoot => _shoot;
        public VolumedAudioClip Explode => _explode;

        [SerializeField]
        private float _accelerateHighPassFreq;
        
        [SerializeField]
        private float _decelerateHighPassFreq;
        
        [SerializeField]
        private VolumedAudioClip _motion;

        [SerializeField]
        private VolumedAudioClip _shoot;
        
        [SerializeField]
        private VolumedAudioClip _explode;
        
    }

}