﻿using Model.Motion;
using Model.Shooting;

namespace View.Player {

    public interface IPlaneEffectsView : IShooter, IShotReceiver, IPlaneMotion {

    }

}