﻿using System;
using UnityEngine;

namespace View.Effects {

    [Serializable]
    public class VolumedAudioClip {

        public float Volume => _volume;
        public AudioClip Sample => _sample;

        [SerializeField]
        private float _volume;

        [SerializeField]
        private AudioClip _sample;

    }

}