﻿namespace View.Effects {

    public interface IPlaneSoundsConfig {

        float AccelerateHighPassFreq { get; }
        
        float DecelerateHighPassFreq { get; }
        
        VolumedAudioClip Motion { get; }
        
        VolumedAudioClip Shoot { get; }
        
        VolumedAudioClip Explode { get; }

    }

}