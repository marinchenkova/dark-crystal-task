﻿using Controller.Audio;
using UnityEngine;
using View.Effects;

namespace View {

    public class PlaneAudioView : MonoBehaviour, IPlaneAudioView {
        
        [SerializeField]
        private AudioSource _motionAudioSource;

        [SerializeField]
        private AudioSource _fxAudioSource;
        
        [SerializeField] 
        private AudioHighPassFilter _filter;
        
        [SerializeField]
        private PlaneSoundsConfig _config;

        private PlaneAudioController _controller;

        private void Awake() {
            _controller = new PlaneAudioController(_motionAudioSource, _fxAudioSource, _filter, _config);
        }

        private void Start() {
            _controller.Initialize();
        }

        private void OnDestroy() {
            _controller.Stop();
        }

        public void OnAccelerate() { 
            _controller.OnAccelerate();            
        }

        public void OnDecelerate() {
            _controller.OnDecelerate();
        }

        public void OnShoot() {
            _controller.OnShoot();
        }

        public void OnShotReceived() {
            _controller.OnShotReceived();
        }
        
    }

}